using MesTestsUnitaires;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Dynamic;
using System.Net.Http;

namespace TestsUnitairesLibrary
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(MaSuperClasse.Multiplier(42), 84);
        }
    }
}
